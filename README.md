# Emulyara ChipperGate - LoRaWAN® compatible gateway emulator

`Under development notice`: project is not fully implemented yet

## About

Emulyara is a project to connect cloud developers with IoT device 
vendors. 

The goal is to have a platform, where backend developers 
could easily test their services against virtual devices 
running as close as possible emulations of real things, provided
by IoT device manufacturers.

ChipperGate is a (planned) gateway to connect virtual devices 
with LoRaWAN® backends via UDP by abstracting as much of protocol 
specifics as possible, leaving to device developers only 
responsibility to implement device business logic.

